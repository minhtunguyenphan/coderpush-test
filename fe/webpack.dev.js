const path = require("path")
const { merge } = require('webpack-merge')
const common = require('./webpack.common.js')

module.exports = merge(common, {
  mode: 'development',
  devtool: 'inline-source-map',
  devServer: {
    contentBase: path.join(__dirname, "public/"),
    host: '127.0.0.1',
    port: 5000,
    hot: true,
    open: true,
    quiet: true,
    inline: true,
    compress: true,
    watchContentBase: true,
    disableHostCheck: true,
    historyApiFallback: true,
    publicPath: "http://localhost:5000/dist/"
  }
})