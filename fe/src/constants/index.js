export const PAGE_TYPE = {
  AUTH: 'AUTH',
  PRIVATE: 'PRIVATE',
  PUBLIC: 'PUBLIC',
  ADMIN: 'ADMIN'
}