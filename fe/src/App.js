import React from 'react'
import AppRouter from '_router'
import 'antd/dist/antd.css'
import '_utils/firebase'
const App = () => {
  return <AppRouter/>
}

export default App