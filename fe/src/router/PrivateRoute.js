import { checkAuth } from '_utils/helpers'
import directRoute from './directRoute'
import { PAGE_TYPE } from '_constants'
const PrivateRoute = directRoute(true, PAGE_TYPE.PRIVATE, '/sign-in')
export default PrivateRoute