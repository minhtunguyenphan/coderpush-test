import React, {useContext} from 'react'
import { UserContext } from "_utils/firebase/providers"
import { Route, Redirect } from 'react-router-dom'
import { PAGE_TYPE } from '_constants'
function directRoute (needAuth, pageType, pathname) {
  return function AppRoute ({ component: Component, layout: Layout, ...rest }) {
    const {user, setUser} = useContext(UserContext);
    return (
      <Route
        {...rest}
        render={props => {
          const PageComponent = () => <Layout {...props}><Component/></Layout>
          switch (pageType){
            case PAGE_TYPE.PUBLIC:
              return <PageComponent/>
            default:
              if (needAuth === true && user) {
                console.log("user", user)
                return <PageComponent/>
              } else if (needAuth === false && !user) {
                console.log("user", user)
                return <PageComponent/>
              }
              return <Redirect to={{ pathname, state: { from: props.location } }}/>
          }
        }}
      />
    )
  }
}

export default directRoute