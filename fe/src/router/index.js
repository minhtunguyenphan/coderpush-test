import React, { useEffect } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import AuthRoutes from '_pages/auth'
// import PublicRoutes from '_pages/public'
import PrivateRoutes from '_pages/private'
import NotFoundPage from "_pages/NotFoundPage"

const MergedRoute = [
  ...AuthRoutes,
  // ...PublicRoutes,
  ...PrivateRoutes
]

const AppRouter = () => {
  useEffect(() => {
  }, [])
  return (
    <Router>
      <Switch>
        {
          MergedRoute ?
            MergedRoute.map(route => {
              const MyRoute = route.route
              return <MyRoute
                exact
                key={route.path}
                path={route.path}
                layout={route.layout}
                component={route.component}
              />
            }) :
            <div>
            </div>
        }
        <Route component={NotFoundPage}/>
      </Switch>

    </Router>
  )
}

export default AppRouter