import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import { functions } from "firebase";

const firebaseConfig = {
  apiKey: process.env.API_KEY,
  authDomain: process.env.AUTH_DOMAIN,
  projectId: process.env.PROJECT_ID,
  databaseURL: process.env.DATABASE_URL,
  storageBucket: process.env.STORAGE_BUCKET,
  messagingSenderId: process.env.MESSAGING_SENDER_ID,
  appId: process.env.APP_ID,
  measurementId: process.env.MEASUREMENT_ID
};

firebase.initializeApp(firebaseConfig);
export const auth = firebase.auth();
export const firestore = firebase.firestore();

export const generateUserDocument = async (user, additionalData) => {
  console.log('generateUserDocument')
  if (!user) return;

  const userRef = firestore.collection("users").doc(`${user.uid}`)
  console.log('userRef', userRef)
  userRef.get().then((doc) => {
    if (doc.exists) {
      console.log("Document data:", doc.data());
      return doc.data()
    } else {
      console.log("No such document!");
      
      console.log('user', user)

      const { email, displayName, photoURL } = user;

      try {
        userRef.set({
          email,
          displayName,
          photoURL,
          ...additionalData
        })
      } catch (error) {
        console.error("Error creating user document", error);
      }

      return getUserDocument(user.uid)
        // doc.data() will be undefined in this case  
    }
  }).catch((error) => {
      console.log("Error getting document:", error);
      return null
  });
};

const getUserDocument = async uid => {
  if (!uid) return null;
  try {
    const userDocument = await firestore.doc(`users/${uid}`).get();

    return {
      uid,
      ...userDocument.data()
    };
  } catch (error) {
    console.error("Error fetching user", error);
  }
};
