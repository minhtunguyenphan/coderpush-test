import React, {useState} from 'react'
import { Link } from 'react-router-dom';
import { Form, Input, Button, Typography, Text } from 'antd';
import { auth, generateUserDocument } from '_utils/firebase';
const { Title } = Typography;
import './style.scss'
const SignUpPage = () => {

  const [error, setError] = useState(null);

  const layout = {
    labelCol: {
      xs: {
        span: 24,
      },
      sm: {
        span: 8,
      },
    },
    wrapperCol: {
      xs: {
        span: 24,
      },
      sm: {
        span: 16,
      },
    },
  };
  
  const tailLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 16,
        offset: 8,
      },
    },
  };

  const onFinish = async (values) => {
    console.log('Success:', values);
    const {email, password} = values
    try{
      const {user} = await auth.createUserWithEmailAndPassword(email, password);
      generateUserDocument(user, {displayName});
    }
    catch(error){
      setError('Error Signing up with email and password');
    }
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <div className="sign-up-page-wrapper">
      <Title style={{margin: "0 0 32px 0"}}>Sign Up</Title>
      <Form
        {...layout}
        name="basic"
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <Form.Item
          label="Email"
          name="email"
          rules={[
            {
              type: 'email',
              message: 'The input is not valid E-mail!',
            },
            { 
              required: true, 
              message: 'Please input your email!' 
            }
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Password"
          name="password"
          rules={[{ required: true, message: 'Please input your password!' }]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item {...tailLayout}>
          <Button type="primary" htmlType="submit">
            Sign Up
          </Button>
        </Form.Item>
      </Form>
      Already have an account?<Link to='/sign-in'>Sign In</Link>
    </div>
  )
}

export default SignUpPage