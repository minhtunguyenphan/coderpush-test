import React, {useState, useEffect,useContext} from 'react'
import { UserContext } from "_utils/firebase/providers"
import { Link, useHistory } from 'react-router-dom';
import { Form, Input, Button, Typography } from 'antd';
import { auth } from '_utils/firebase';
import {generateUserDocument} from '_utils/firebase'
const { Title } = Typography;
import './style.scss'
const SignInPage = () => {

  const history = useHistory();
  const [error, setError] = useState(null);
  const {user, setUser} = useContext(UserContext)
  useEffect(() => {
    console.log('user', user)
    if(user) {
      history.push("/share-video")
    }
  }, [user])

  const layout = {
    labelCol: {
      xs: {
        span: 24,
      },
      sm: {
        span: 8,
      },
    },
    wrapperCol: {
      xs: {
        span: 24,
      },
      sm: {
        span: 16,
      },
    },
  };
  
  const tailLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 16,
        offset: 8,
      },
    },
  };

  const onFinish = (values) => {
    console.log('Success:', values);
    const {email, password} = values
    auth.signInWithEmailAndPassword(email, password).then(async userCredential => {
      console.log("userCredential", userCredential)
      const userInfo = await generateUserDocument(userCredential.user)
      console.log("user", userInfo)
      setUser({userInfo})
    }).catch(error => {
      setError("Error signing in with password and email!");
        console.error("Error signing in with password and email", error);
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <div className="sign-in-page-wrapper">
      <Title style={{margin: "0 0 32px 0"}}>Sign In</Title>
      <Form
        {...layout}
        name="basic"
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <Form.Item
          label="Email"
          name="email"
          rules={[
            {
              type: 'email',
              message: 'The input is not valid E-mail!',
            },
            { 
              required: true, 
              message: 'Please input your email!' 
            }
          ]}
        >
          <Input/>
        </Form.Item>

        <Form.Item
          label="Password"
          name="password"
          rules={[{ required: true, message: 'Please input your password!' }]}
        >
          <Input.Password/>
        </Form.Item>

        <Form.Item {...tailLayout}>
          <Button type="primary" htmlType="submit">
            Sign In
          </Button>
        </Form.Item>
      </Form>
      Don't have an account?<Link to='/sign-up'>Sign Up</Link>
    </div>
  )
}

export default SignInPage