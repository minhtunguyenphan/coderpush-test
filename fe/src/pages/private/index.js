import SwipePage from './SwipePage'

import PrivateRoute from '_router/PrivateRoute'
import PrivateLayout from '_layout/private'

const routeConfig = {
  layout: PrivateLayout,
  route: PrivateRoute
}

const PrivateRoutes = [
  {
    ...routeConfig,
    title: 'Swipe Page',
    component: SwipePage,
    path: '/',
  }
]

export default PrivateRoutes